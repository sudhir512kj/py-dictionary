import json
import difflib
from difflib import get_close_matches

data = json.load(open('dictionary.json'))


def retrieve_definition(word):
    word = word.lower()
    if word in data:
        return data[word]
    elif word.title() in data:
        return data[word.title()]
    elif word.upper() in data:
        return data[word.upper()]
    elif len(get_close_matches(word, data.keys())) > 0:
        action = input("Did you mean %s instead? [y or n]:" % get_close_matches(
            word, data.keys())[0])
        if (action == 'y' or action == 'yes'):
            return data[get_close_matches(word, data.keys())[0]]
        elif (action == 'n' or action == 'no'):
            return ("The word doesn't exist, yet.")
        else:
            return ("We don't understand your entry. Apologies.")


# Input from user
count = 1
while count:
    word_user = input("Enter a word: ")
    output = retrieve_definition(word_user)

    if type(output) == list:
        for item in output:
            print("-", item)

    else:
        print("-", output)

    newWord = input("Want to find another word ? [y or n]:")
    if newWord == 'y' or newWord == 'yes':
        count = 1
    else:
        count = 0
